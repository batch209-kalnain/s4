const { assert } = require('chai');
const { getCircleArea,getNumberOfChar } = require('../src/util.js');

describe('test get circle area',() => {
	it('test area of circle radius 15 is 706.86',() => {
		let area = getCircleArea(15);
		assert.equal(area,706.86);
	})

	it('test area of circle neg1 radius is undefined',() => {
		let area = getCircleArea(-1);
		assert.isUndefined(area);
	})

	it('test area of circle 0 radius is undefined',() => {
		let area = getCircleArea(0);
		assert.isUndefined(area);
	})

	it('test area of circle non numerical radius is undefined',() => {
		let area = getCircleArea('one');
		assert.isUndefined(area);
	})

	it('test area of circle invalid type is undefined',() => {
		let area = getCircleArea({number:25});
		assert.isUndefined(area);
	})
})

describe('test get nummber of char in sentence',() => {
	it('test number of l in string is 3',() => {
		let numChar = getNumberOfChar("l","Hello World");
		assert.equal(numChar,3);
	})

	it('test number of a in string is 2',() => {
		let numChar = getNumberOfChar("a","Malta");
		assert.equal(numChar,2);
	})

	it('test get number of character for non-string first argument is undefined',() => {
		let numChar = getNumberOfChar(2,"Hello");
		assert.isUndefined(numChar);
	})

	it('test get number of character for non-string second argument is undefined',() => {
		let numChar = getNumberOfChar(2,"Hello");
		assert.isUndefined(numChar);
	})
})