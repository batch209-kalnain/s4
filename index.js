const express = require('express');
const app = express();
const port = process.env.PORT || 4000;

app.use(express.json());

let userRoutes = require('./routes/routes');
app.use('/users',userRoutes);

app.listen(port,() => console.log("Express is running at port " + port))