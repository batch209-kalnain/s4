function getCircleArea(radius) {
	if(radius <= 0 || isNaN(radius)) {
		return undefined;
	}

	return 3.1416 * (radius**2);
}

function getNumberOfChar(char,string) {
	if(typeof string !== 'string' || typeof char !== 'string'){
		return undefined;
	}
	let characters = string.split("");
	let counter = 0;
	characters.forEach(character => {
		if(character === char) {
			counter++;
		}
	})
	return counter;
}

let users = [
	{
		username:"brBoyd87",
		password:"87brandon19"
	},
	{
		username:"tylerOfsteve",
		password:"stevenstyle75"
	}
]

module.exports = {
	getCircleArea,
	getNumberOfChar,
	users
}