const { users } = require('../src/util.js');
const express = require("express");
const router = express.Router();


router.post('/register', (req, res) => {
	if(!req.body.hasOwnProperty('name')){
		return res.status(400).send({
			'error' : 'Bad Request : missing required parameter NAME'
		})
	}
	
	if(!req.body.hasOwnProperty('age')){
		return res.status(400).send({
			'error' : 'Bad Request: missing required parameter AGE'
		})
	}

	if(!req.body.hasOwnProperty('username')){
		return res.status(400).send({
			'error' : 'Bad Request: missing required parameter USERNAME'
		})
	}

	if(typeof req.body.age !== 'number'){
		return res.status(400).send({
			'error' : 'Bad Request: AGE has to be a number'
		})
	}

	if(typeof req.body.name !== 'string'){
		return res.status(400).send({
			'error' : 'Bad Request : NAME has to be string' 
		})
	}

})



router.post('/login',(req,res)=>{

	let foundUser = users.find((user) => {

		return user.username === req.body.username && user.password === req.body.password

	});

	if(!req.body.hasOwnProperty('username')){
		return res.status(400).send({
			'error' : 'Bad Request: missing required parameter USERNAME'
		})
	}

	if(!req.body.hasOwnProperty('password')){
		return res.status(400).send({
			'error' : 'Bad Request: missing required parameter PASSWORD'
		})
	}

	if(foundUser){
		return res.status(200).send({
			'success': 'Thank you for logging in.'
		})
	}else{
		return res.status(403).send({
			'error': 'Incorrect credential.'
		})
	}

})

module.exports = router;
